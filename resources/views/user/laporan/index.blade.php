@extends('user.layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="col-sm-6">
                <h1>Fp Laporan</h1>
            </div>
            <div class="card mt-3">
                <div class="card-body">
                    <div class="col-sm-6">
                        <table>
                            <tr>
                                <td>NIP</td>
                                <td>: <?= $biodata->userid;?></td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td>: <?= $biodata->name;?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="card mt-2">
                <div class="card-body">
                    <form action="" method="post" class="form-group">
                        @csrf
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bulan">Bulan</label>
                                <select name="bulan" id="bulan" class="form-control">


                                    <?php
                                    for ($i=0; $i < 12; $i++) {
                                       ?>
                                       <option value="<?= '0'.$i+1;?>" @if('0'.$i+1 === $bulanSelected) selected @endif><?= $bulan[$i];?></option>
                                    <?php }

                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label for="tahun">Tahun</label>
                                <select name="tahun" id="tahun" class="form-control">
                                <option value="{{ $tahunSelected }}" class="form-control">{{ $tahunSelected }}</option>
                                    <?php
                                    $now = date('Y');

                                    for ($i = 0; $i < 10; $i++) {
                                        ?>
                                        <option value="<?= $now - $i;?>"><?= $now - $i;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-3 mt-4">
                                <button type="submit" class="btn btn-primary mt-2">search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body">
                @include('user.laporan.table')

                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

