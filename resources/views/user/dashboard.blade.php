@extends('user.layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">

        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-body">
                    <div class="text-center">
                        <h5> {{ $tanggal }}</h5>
                        <h5 id="txt"></h5>
                        <p id="absen"></p>
                    </div>

                        <form action="{{ route('absen') }}" class="form-group" method="post">
                            @csrf
                            <div class="mt-4 row d-flex align-items-center justify-content-center" style="margin-left:10% ">

                                <?php 
                                if(!$absenMasuk){?>
                                    <div class="col-sm-3">
                                        <button class="btn btn-primary" name="absenMasuk" value="true" id="masuk" <?= ($absenMasuk) ? "hidden" : "" ;?>>Absen Masuk</button>
                                    </div>
                                <?php } 

                                if(!$absenPulang && $absenMasuk){?>
                                <div class="col-sm-3">
                                    <button class="btn btn-success" name="absenPulang" value="true" id="pulang" disabled>Absen pulang</button>
                                </div>

                                <?php } ?>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    

    function startTime() {
      const today = new Date();
      let h = today.getHours();
      let m = today.getMinutes();
      let s = today.getSeconds();
      m = checkTime(m);
      s = checkTime(s);
      document.getElementById('txt').innerHTML =  h + ":" + m + ":" + s;
      setTimeout(startTime, 1000);
    }
    
    function checkTime(i) {
      if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
      return i;
    }
</script>

<script>
    // Set the date we're counting down to
    // document.getElementById("pulang").removeAttribute('disabled');

    var akhirAbsensi = '<?= $akhirAbsensi?>'
    var countDownDate = new Date(akhirAbsensi).getTime();
    
    // Update the count down every 1 second
    var x = setInterval(function() {
    
      // Get today's date and time
      var now = new Date().getTime();
    
      // Find the distance between now and the count down date
      var distance = countDownDate - now;
    
      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
      // Display the result in the element with id="absen"
      document.getElementById("absen").innerHTML = days + "d " + hours + "h "
      + minutes + "m " + seconds + "s ";
    
      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("absen").innerHTML = "ABSENSI PULANG";
        document.getElementById("pulang").removeAttribute('disabled');
        document.getElementById("masuk").setAttribute('disabled', '');
      }
    }, 1000);
</script>
@endsection
