<li class="nav-item">
    <a href="{{ route('dashboard') }}"
       class="nav-link {{ Request::is('user/dashboard*') ? 'active' : '' }}">
       <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>Dashboard</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route ('laporan', $id) }}"
       class="nav-link {{ Request::is('user/laporan*') ? 'active' : '' }}">
       <i class="nav-icon fas fa-edit"></i>
        <p>Laporan Kehadiran</p>
    </a>
</li>




