@section('third_party_stylesheets')
    @include('layouts.datatables_css')
@endsection

<div class="table-responsive">
    <table class="table" id="myTable">
        <thead>
            <tr>
                <th>No</th>
        <th>Tanggal</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            ?>
        @foreach($viewDetailAttendances as $viewDetailAttendance)
            <tr>
                <td>{{ $no++ }}</td>
            <td>{{ $viewDetailAttendance->tanggal }}</td>
                
            </tr>
        @endforeach
        </tbody>
    </table>
</div>



@section('third_party_scripts')
    @include('layouts.datatables_js')
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>
@endsection 





