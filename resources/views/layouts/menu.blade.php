<li class="nav-item">
    <a href="{{ route('home') }}"
       class="nav-link {{ Request::is('home*') ? 'active' : '' }}">
       <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>Dashboard</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('fpUsers.index') }}"
       class="nav-link {{ Request::is('fpUsers*') ? 'active' : '' }}">
       <i class="nav-icon fas fa-users"></i>
        <p>Data Dosen</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('fpAttendances.index') }}"
       class="nav-link {{ Request::is('fpAttendances*') ? 'active' : '' }}">
       <i class="nav-icon fas fa-clock"></i>
        <p>Daftar Kehadiran</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route ('laporan.index') }}"
       class="nav-link ">
       <i class="nav-icon fas fa-edit"></i>
        <p>Laporan Kehadiran</p>
    </a>
</li>


{{-- <li class="nav-item">
    <a href="{{ route('viewDetailAttendances.index') }}"
       class="nav-link {{ Request::is('viewDetailAttendances*') ? 'active' : '' }}">
        <p>View Detail Attendances</p>
    </a>
</li> --}}



