@extends('absen_dosen.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="col-sm-6">
                <h1>Fp Laporan</h1>
            </div>
            <div class="card mt-3">
                <div class="card-body">
                    <div class="col-sm-6">
                        <table>
                            <tr>
                                <td>NIP</td>
                                <td>: </td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td>: </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body">
            @section('third_party_stylesheets')
                @include('layouts.datatables_css')
            @endsection

            <div class="table-responsive">
                <table class="table" id="myTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        ?>
                        {{-- @foreach ($viewDetailAttendances as $viewDetailAttendance)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $viewDetailAttendance->tanggal }}</td>

                            </tr>
                        @endforeach --}}
                    </tbody>
                </table>
            </div>



            @section('third_party_scripts')
                @include('layouts.datatables_js')
                <script>
                    $(document).ready(function() {
                        $('#myTable').DataTable();
                    });
                </script>
            @endsection

            <div class="card-footer clearfix float-right">
                <div class="float-right">

                </div>
            </div>
        </div>

    </div>
</div>

@endsection
