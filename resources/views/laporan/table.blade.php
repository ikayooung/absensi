<div class="table-responsive">
    <table class="table" id="fpLaporan-table">
        <thead>
            <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Name</th>
                <th>Cek Absensi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; ?>
        @foreach ($laporan as $item)
        <?php $no++; ?>
        <tr>
            <td>{{ $no }}</td>
            <td>{{$item->userid}}</td>
            <td>{{$item->name}}</td>
            <td>
                <a href="<?= route('viewDetailAttendace', $item->id);?>" class="btn btn-block btn-primary btn-sm">Lihat Kehadiran</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
