<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::post('login_test', [App\Http\Controllers\Auth\LoginController::class, 'authenticate']);

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/finger', [App\Http\Controllers\HomeController::class, 'finger'])->name('finger');

    Route::resource('fpUsers', App\Http\Controllers\FpUserController::class);
    Route::resource('fpAttendances', App\Http\Controllers\FpAttendanceController::class);
    Route::get('attendancetest', [App\Http\Controllers\HomeController::class, 'attendancetest']);
    Route::get('fpAttendances_sync', [App\Http\Controllers\FpAttendanceController::class, 'fpAttendances_sync'])->name('fpAttendances.sync');
    Route::get('fpUsers_sync', [App\Http\Controllers\FpUserController::class, 'fpuser_sync'])->name('fpUsers.sync');


    Route::resource('laporan', App\Http\Controllers\LaporanController::class);

    //view detail laporan
    Route::any('viewDetailAttendace/{id}', [App\Http\Controllers\ViewDetailAttendanceController::class, 'index'])->name('viewDetailAttendace');
});


Route::get('/user/dashboard', [App\Http\Controllers\User\homeController::class, 'index'])->name('dashboard')->middleware('is_user');
Route::any('/user/laporan/{id}', [App\Http\Controllers\User\laporanController::class, 'index'])->name('laporan')->middleware('is_user');
Route::any('/user/absen', [App\Http\Controllers\User\absenController::class, 'store'])->name('absen')->middleware('is_user');
Route::get('/user/login', [App\Http\Controllers\User\LoginUserController::class, 'index'])->name('user.login');
Route::post('/user/authenticate', [App\Http\Controllers\User\LoginUserController::class, 'authenticate'])->name('user.authenticate');




Route::resource('fpViewAttendances', App\Http\Controllers\FpViewAttendanceController::class);
