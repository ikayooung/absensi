<?php

namespace App\Repositories;

use App\Models\ViewDetailAttendance;
use App\Repositories\BaseRepository;

/**
 * Class ViewDetailAttendanceRepository
 * @package App\Repositories
 * @version April 11, 2023, 10:52 am WITA
*/

class ViewDetailAttendanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'no',
        'tanggal'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ViewDetailAttendance::class;
    }
}
