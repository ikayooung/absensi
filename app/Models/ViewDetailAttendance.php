<?php

namespace App\Models;

// use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ViewDetailAttendance
 * @package App\Models
 * @version April 11, 2023, 10:52 am WITA
 *
 * @property bigint $no
 * @property string $tanggal
 */
class ViewDetailAttendance extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'fp_attendance';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'no',
        'tanggal'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tanggal' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
