<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    use HasFactory;
    protected $table = 'fp_user';
    protected $guarded = ['uid'];
    public function userId(){
        return $this->belongsTo(FpAttendance::class, 'id');
    }
}
