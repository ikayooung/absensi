<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;


class IsUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::get('userid')) {
            return $next($request);
        }

        return redirect("user/login")->with("error", "You don't have user access.");
    }
}
