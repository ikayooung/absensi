<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\FpUser;
use Illuminate\Http\Request;

class LoginUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function index()
    {
        return view('fp_users.login');
    }

    public function authenticate(Request $request)
    {
        // dd($request->all());
        $fp_users_model = new FpUser();

        $check = $fp_users_model
            ->where(['userid' => $request->userid, 'id' => $request->password])
            ->first();
        // dd($check);
        if ($check) {
            // dd('haloo');
            $request->session()->put('userid', $check->userid);
            $request->session()->put('uid', $check->id);

            return redirect('user/dashboard');
        }

        return redirect()->back()->with('failed', 'Ooppss!!');
    }
}
