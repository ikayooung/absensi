<?php

namespace App\Http\Controllers\User;

use App\Repositories\ViewDetailAttendanceRepository;
use App\Http\Controllers\Controller;
use App\Models\FpAttendance;
use App\Models\FpUser;
use App\Models\Laporan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Session;
use Response;

class laporanController extends Controller
{
    
    /** @var ViewDetailAttendanceRepository $viewDetailAttendanceRepository*/
    private $viewDetailAttendanceRepository;

    protected $FpAttendanceModel;

    protected $FpUsersModel;

    public function __construct(ViewDetailAttendanceRepository $viewDetailAttendanceRepo)
    {
        $this->viewDetailAttendanceRepository = $viewDetailAttendanceRepo;

        $this->FpAttendanceModel = new FpAttendance();
        $this->FpUsersModel = new FpUser();
    }

    /**
     * Display a listing of the ViewDetailAttendance.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index($id, Request $request)
    {
        if(!Session::get('userid')){
            return redirect('/login');
        }
        
        $bulan = ($request->bulan) ? $request->bulan : date('m') ;
        $tahun = ($request->tahun) ? $request->tahun : date('Y') ;

        $viewDetailAttendances = $this->FpAttendanceModel->where([
            'id' => $id,
        ])
        ->whereMonth('timestamp', $bulan)
        ->whereYear('timestamp', $tahun)
        ->orderBy('timestamp', 'ASC')
        ->get();

        $biodata = $this->FpUsersModel->where([
            'id' => $id
        ])->first();

        $namaBulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei',
                    'Juni', 'Juli', 'Agustus', 'September', 'Oktober',
                    'November', 'Desember'];

        $data = [
            'viewDetailAttendances' => $viewDetailAttendances,
            'biodata' => $biodata,
            'bulan' => $namaBulan,
            'bulanSelected' => $bulan,
            'tahunSelected' => $tahun,
            'name' => $biodata->name,
            'id' => $biodata->id
        ];

        // dd($viewDetailAttendances);
        return view('user/laporan.index', $data)
            ->with('viewDetailAttendances', $viewDetailAttendances);
    }

}
