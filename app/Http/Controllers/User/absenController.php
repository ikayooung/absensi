<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\FpAttendance;
use App\Models\FpUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class absenController extends Controller
{
    protected $fpAttendanceModel;
    protected $fpUserModel;

    public function __construct()
    {
        $this->fpAttendanceModel = new FpAttendance();
        $this->fpUserModel = new FpUser();
    }

    public function store(Request $request)
    {
        
        $user = $this->fpUserModel->where('userid', Session::get('userid'))->first();

        // validasi apakah sudah absen atau belum
        
        // dd($request->absenMasuk);
        if($request->absenMasuk){
            // lakukan validasi apakah masih waktu absen masuk atau tidak

            $target_time = strtotime('16:00:00');
            $current_time = time();

            $remaining_time = $target_time - $current_time;

            // $days = floor($remaining_time / (24 * 60 * 60));
            // $hours = floor(($remaining_time % (24 * 60 * 60)) / (60 * 60));
            // $minutes = floor(($remaining_time % (60 * 60)) / 60);
            // $seconds = $remaining_time % 60;

            // $remaining_time_string = sprintf('%d days, %d hours, %d minutes, %d seconds', $days, $hours, $minutes, $seconds);

            if($remaining_time < 0){
                return redirect()->back()->with('failed', 'Ooppss!! Waktu absen masuk berakhir!');
            }

            //input absensi 
            $data = [
                'id' => $user->id,
                'uid' => $user->id,
                'type' => 1,
                'state' => 0,
                'mesin' => 1,
                'timestamp' => date('Y-m-d H:i:s')
            ];
    
            // if($this->fpAttendanceModel->create($data)){
            //     dd('hsajas');
            // }
    
            $r = FpAttendance::create($data);

            // dd($r)
            return redirect()->back()->with('success', 'Absen Masuk Berhasil!!');
        }

        if($request->absenPulang){
            //input absensi 
            $data = [
                'id' => $user->id,
                'uid' => $user->id,
                'type' => 1,
                'state' => 0,
                'mesin' => 1,
                'timestamp' => date('Y-m-d H:i:s')
            ];
    
            // if($this->fpAttendanceModel->create($data)){
            //     dd('hsajas');
            // }
    
            $r = FpAttendance::create($data);

            // dd($r)
            return redirect()->back()->with('success', 'Absen Pulang Berhasil!!');
        }
        

        return redirect()->back();
    }
}
