<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Models\FpAttendance;
use App\Models\FpUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class homeController extends Controller
{
    protected $userid;
    protected $model;
    protected $fpAttendanceModel;

    public function __construct()
    {
        $this->model = new FpUser();
        $this->fpAttendanceModel = new FpAttendance();
        $this->middleware(function ($request, $next) {
            $this->userid = Session::get('userid');

            return $next($request);
        });
    }



    public function index()
    {
        $this->userid = Session::get('userid');

        if (!Session::get('userid')) {
            return redirect('/login');
        }

        // ubah 16:00:00 untuk menentukan akhir dari jam absensi
        $akhirAbsensi = date('Y-m-d') . ' 16:00:00';
        $user = $this->model->where('userid', $this->userid)->first();

        //cek udah absen belum
        $cek = $this->fpAttendanceModel->where([
            'id' => $user->id
        ])
            ->whereYear('timestamp', date('Y'))
            ->whereMonth('timestamp', date('m'))
            ->whereDay('timestamp', date('d'))
            ->get();
        // dd($cek);
        $masuk = (count($cek)) ? true : false;
        $pulang = (count($cek) > 1) ? true : false;

        // dd(count($cek));
        // dd($masuk);
        $data = [
            'name' => $user->name,
            'tanggal' => Carbon::parse(date('Y-m-d'))
                ->translatedFormat('l, d F Y'),
            'akhirAbsensi' => $akhirAbsensi,
            'absenMasuk' => $masuk,
            'id' => $user->id,
            'absenPulang' => $pulang
        ];



        return view('user/dashboard', $data);
    }
}
