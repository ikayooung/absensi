<?php

namespace App\Http\Controllers;

use App\DataTables\FpViewAttendanceDataTable;
use App\Http\Requests\CreateViewDetailAttendanceRequest;
use App\Http\Requests\UpdateViewDetailAttendanceRequest;
use App\Repositories\ViewDetailAttendanceRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\FpAttendance;
use App\Models\FpUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Response;

class ViewDetailAttendanceController extends AppBaseController
{
    /** @var ViewDetailAttendanceRepository $viewDetailAttendanceRepository*/
    private $viewDetailAttendanceRepository;

    protected $FpAttendance;

    protected $FpUsers;

    public function __construct(ViewDetailAttendanceRepository $viewDetailAttendanceRepo)
    {
        $this->viewDetailAttendanceRepository = $viewDetailAttendanceRepo;

        $this->FpAttendance = new FpAttendance();
        $this->FpUsers = new FpUser();
    }

    /**
     * Display a listing of the ViewDetailAttendance.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index($id, Request $request)
    {
        $bulan = ($request->bulan) ? $request->bulan : date('m') ;
        $tahun = ($request->tahun) ? $request->tahun : date('Y') ;

        $viewDetailAttendances = $this->FpAttendance->where([
            'id' => $id,
        ])
        ->whereMonth('timestamp', $bulan)
        ->whereYear('timestamp', $tahun)
        ->orderBy('timestamp', 'ASC')
        ->get();

        $biodata = $this->FpUsers->where([
            'id' => $id
        ])->first();

        $namaBulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei',
                    'Juni', 'Juli', 'Agustus', 'September', 'Oktober',
                    'November', 'Desember'];

        $data = [
            'viewDetailAttendances' => $viewDetailAttendances,
            'biodata' => $biodata,
            'bulan' => $namaBulan,
            'bulanSelected' => $bulan,
            'tahunSelected' => $tahun
        ];

        // dd($viewDetailAttendances);
        return view('view_detail_attendances.index', $data)
            ->with('viewDetailAttendances', $viewDetailAttendances);
    }

}
